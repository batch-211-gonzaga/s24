// #3
const getCube = 3 ** 3;

// #4
console.log(`The cube of 3 is ${getCube}.`);

// #5
const address = ['Camia Rd', 'Brgy Cutcut', 'Angeles City'];

// #6
const [road, brgy, city] = address;
console.log(`Address is ${road}, ${brgy}, ${city}.`);

// #7
const animal = {
  genus: 'Ursus',
  species: 'arctos',
  weight: 152,
  height: 2
}

// #8
const {genus, species, weight, height} = animal;
console.log(`The ${genus} ${species} weighs approximately ${weight} kg and measures ${height} meters from head to toe.`);

// #9
const numArray = [1, 2, 3, 4, 5]

// #10
numArray.forEach((n) => console.log(n));

// #11
const reduceNumber = numArray.reduce((x, y) => x + y);
console.log(reduceNumber);

// #12
class Dog {
  constructor (name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

// #13
const bantay = new Dog('Bantay', 11, 'aspin');
console.log(bantay);
