// ES6 Updates

// let, const
let name = 'John';

// Exponent operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

let string1 = 'fun';
let string2 = 'Bootcamp';
let string3 = 'Coding';
let string4 = 'JavaScript';
let string5 = 'Zuitt';
let strint6 = 'Learning';
let string7 = 'love';
let string8 = 'I';
let string9 = 'is';
let string10 = 'in';

/*
    1. Create new vars sentence1 and sentence2
    2. Concatenate and save a resulting string into sentence1
    3. Concatenate and save a resulting string into sentence2
      log both vars in the console and take a s/s
      The sentences must have spaces and punctuation
*/

const sentence1 = string8 + ' ' + string7 + ' ' + string3 + ' ' + string10 + ' ' + string4 + '.';
const sentence2 = string5 + ' ' + string3 + ' ' + string2 + ' ' + string9 + ' ' + string1 + '.';

//console.log(sentence1);
//console.log(sentence2);

// Template literals allow us to create strings using `` and easily embed JavaScript expressions

let sentence = `${string8} ${string7} ${string5} ${string8} ${string2}!`;
console.log(sentence);

/*
  ${} placeholder used to embed js expressions when creating strings using template literals
*/

// Pre-Template Literal String
// ("") or ('')

let message = 'Hello' + name + '! Welcome to programming!';

// Strings using template literals
// (``) backticks

message = `Hello ${name}! Welcome to programming!`;
console.log(message);

// Multi-line using Template Literals

const anotherMessage = `
${name} attended a math competitaion.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
`
console.log(anotherMessage);

let dev = {
  name: 'Peter',
  lastName: 'Parker',
  occupation: 'web developer',
  income: 50000,
  expenses: 60000,
}

console.log(`${dev.name} is a ${dev.occupation}`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

// Array Destructuring
    //- Allows us to unpack elements in arrays into distinct variables

const fullName = ['Juan', 'Dela', 'Cruz'];

// Pre-array destructuring

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you.`);


const [firstName, middleName, lastName] = fullName;
//const [firstName, , lastName] = fullName; <- blanks also allowed
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you.`);

// Object Destructuring
/*
  Allows us to unpack properties of objects into distinct variables
*/

const person = {
  givenName: 'Jane',
  maidenName: 'Dela',
  familyName: 'Cruz'
};

// Pre-Oject Destructuring
// We can access them using . or []

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Object Destructuring

const {givenName, maidenName, familyName} = person;

// Arrow functions
// Compact alternative syntax to traditional functions

const hello = () => {
  console.log('Hello world!');
}

hello();

const printFullName = (firstName, middleInitial, lastName) => {
  console.log(`${firstName} ${middleInitial}, ${lastName}`);
};

printFullName('John', 'D', 'Smith');

const students = ['John', 'Jane', 'Judy']
students.forEach(student => {
  console.log(student);
});

//students.forEach((student) => console.log(`${student} is a student.`));

// Implicit return statement
// only works on arrow functions without {}

// pre-arrow function

const add = (x, y) => x + y;

let total = add(1,2);
console.log(total);

/*
    Mini-activity
      create a subtract, multiple, divide arrow function
      log
*/

const subtract = (x, y) => x - y;
//console.log(subtract(6, 4));

const mutliply = (x, y) => x * y;
//console.log(mutliply(2, 4));

const divide = (x, y) => x / y;
//console.log(divide(8, 4));

// Default function parameters

// Class-Based Object Blueprint

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

